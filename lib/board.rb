class Board

  attr_accessor :grid

  def self.default_grid
    Array.new(10) { Array.new(10) }
  end

  def initialize(grid=nil)
    @grid = grid || Board.default_grid
  end

  def count
    arr = []
    self.grid.flatten.each do |el|
      arr << el if el == :s
    end
    arr.count
  end

  def empty?(pos = nil)
    if pos
      x = pos[0]
      y = pos[-1]
      if @grid[x][y] == :s
        return false
      else
        return true
      end
    else
      return false if count > 0
    end
    true
  end

  def full?
    return true if count == @grid.count**2 && count != 0
    # @grid.flatten.each do |el|
    #   return false if el == nil
    # end
    # true
  end

  def won?
    return false if count != 0
    true
  end

  def populate_grid
    size = @grid.count
    random_posx = rand(size+1)
    random_posy = rand(size+1)
    size.times { @grid[random_posx, random_posy] = :s }
  end

  def place_random_ship
     if self.full?
       raise "error"
     elsif self.empty?
       populate_grid until self.full?
     end
     @grid
  end

  def [](pos)
    row, col = pos
    @grid[row][col]
  end

  def []=(pos, mark)
    row, col = pos
    @grid[row][col] = mark
  end


end
