class BattleshipGame
  # attr_reader :board, :player
  attr_accessor :grid, :board, :player

  def initialize(player = HumanPlayer.new("Bob"), board = Board.new)
    @player = player
    @board = board
    @grid = grid
  end

  def attack(pos)
    board[pos] = :x
  end

  def count
    @board.count
  end

  def game_over?
    if @board.won?
      return true
    else
      return false
    end
  end

  def play_turn
    target = @player.get_play
    attack(target)
  end


end


# if $PROGRAM_NAME == __FILE__
#   BattleshipGame.new.play
# end
